// import { generateQR } from './util'
// import { PDFDocument, rgb, PDFLib.StandardFonts } from 'pdf-lib'

const ys = {
    travail: 488,
    achats: 417,
    sante: 347,
    famille: 325,
    handicap: 291,
    sport_animaux: 269,
    convocation: 199,
    missions: 178,
    enfants: 157,
}
// 
async function generatePdf(profile, pdfBase) {
    // alert("alerte -1")
    const reasons = ys["sport_animaux"]
    // print()
    // alert(timestamp_to_datehour(false_time_of_creation));
    creationInstant = new Date()
    creationInstant = new Date(creationInstant - random_number_generator(20 * 60 * 1000, 30 * 60 * 1000));
    const creationDate = creationInstant.toLocaleDateString('fr-FR')
    const creationHour = creationInstant
        .toLocaleTimeString('fr-FR', { hour: '2-digit', minute: '2-digit' })
        .replace(':', 'h')

    const datesortie = creationDate
    const heuresortie = creationHour
    // alert(creationDate, creationHour)
    const {
        lastname,
        firstname,
        // birthday,
        placeofbirth,
        address,
        zipcode,
        city,

    } = profile
    let { birthday } = profile
    birthday = new Date(birthday)
    birthday = birthday.toLocaleDateString("fr-FR")

    const data = [
        `Cree le: ${creationDate} a ${creationHour}`,
        `Nom: ${lastname}`,
        `Prenom: ${firstname}`,
        `Naissance: ${birthday} a ${placeofbirth}`,
        `Adresse: ${address} ${zipcode} ${city}`,
        `Sortie: ${datesortie} a ${heuresortie}`,
        `Motifs: sport_animaux`,
        '', // Pour ajouter un ; aussi au dernier élément
    ].join(';\n')
    // console.log(data)

    const existingPdfBytes = await fetch(pdfBase).then((res) => res.arrayBuffer())
    // alert("alerte 0")

    const pdfDoc = await PDFLib.PDFDocument.load(existingPdfBytes)
    // alert("alerte .5")

    // set pdf metadata
    pdfDoc.setTitle('COVID-19 - Déclaration de déplacement')
    pdfDoc.setSubject('Attestation de déplacement dérogatoire')
    pdfDoc.setKeywords([
        'covid19',
        'covid-19',
        'attestation',
        'déclaration',
        'déplacement',
        'officielle',
        'gouvernement',
    ])
    pdfDoc.setProducer('DNUM/SDIT')
    pdfDoc.setCreator('')
    pdfDoc.setAuthor("Ministère de l'intérieur")

    const page1 = pdfDoc.getPages()[0]

    const font = await pdfDoc.embedFont(PDFLib.StandardFonts.Helvetica)
    const drawText = (text, x, y, size = 11) => {
        page1.drawText(text, { x, y, size, font })
    }

    drawText(`${firstname} ${lastname}`, 107, 657)
    drawText(birthday, 107, 627)
    drawText(placeofbirth, 240, 627)
    drawText(`${address} ${zipcode} ${city}`, 124, 596)

    // reasons
    //     .split(', ')
    //     .forEach(reason => {
    //         drawText('x', 59, ys[reason], 12)
    //     })
    drawText('x', 59, reasons, 12)

    // alert("alerte 1")
    let locationSize = getIdealFontSize(font, profile.city, 83, 7, 11)

    if (!locationSize) {
        alert(
            'Le nom de la ville risque de ne pas être affiché correctement en raison de sa longueur. ' +
            'Essayez d\'utiliser des abréviations ("Saint" en "St." par exemple) quand cela est possible.',
        )
        locationSize = 7
    }
    // alert("alerte 2")

    drawText(profile.city, 93, 122, locationSize)
    drawText(`${datesortie}`, 76, 92, 11)
    drawText(`${heuresortie}`, 246, 92, 11)

    const shortCreationDate = `${creationDate.split('/')[0]}/${creationDate.split('/')[1]
        }`
    // drawText(shortCreationDate, 314, 189, locationSize)

    // // Date création
    // drawText('Date de création:', 479, 130, 6)
    // drawText(`${creationDate} à ${creationHour}`, 470, 124, 6)

    const qrTitle1 = 'QR-code contenant les informations '
    const qrTitle2 = 'de votre attestation numérique'

    const generatedQR = await generateQR(data)

    const qrImage = await pdfDoc.embedPng(generatedQR)
    // alert("alerte 3")

    page1.drawText(qrTitle1 + '\n' + qrTitle2, { x: 415, y: 135, size: 9, font, lineHeight: 10, color: PDFLib.rgb(1, 1, 1) })

    page1.drawImage(qrImage, {
        x: page1.getWidth() - 156,
        y: 25,
        width: 92,
        height: 92,
    })

    pdfDoc.addPage()
    const page2 = pdfDoc.getPages()[1]
    page2.drawText(qrTitle1 + qrTitle2, { x: 50, y: page2.getHeight() - 70, size: 11, font, color: PDFLib.rgb(1, 1, 1) })
    page2.drawImage(qrImage, {
        x: 50,
        y: page2.getHeight() - 390,
        width: 300,
        height: 300,
    })

    const pdfBytes = await pdfDoc.save()

    // console.log("juste avant le return new blob")
    return new Blob([pdfBytes], { type: 'application/pdf' })
}

function getIdealFontSize(font, text, maxWidth, minSize, defaultSize) {
    let currentSize = defaultSize
    let textWidth = font.widthOfTextAtSize(text, defaultSize)

    while (textWidth > maxWidth && currentSize > minSize) {
        textWidth = font.widthOfTextAtSize(text, --currentSize)
    }

    return textWidth > maxWidth ? null : currentSize
}


async function création_PDF(adresse, code_postal, ville) {
    console.log("création PDF :")
    console.log(adresse)
    console.log(code_postal)
    console.log(ville)

    $("#btn_gen_pdf").text("Création du PDF en cours…");
    // alert()//
    const profile = {
        lastname: $("#nom_de_famille").val(),
        firstname: $("#prénom").val(),
        birthday: $("#date_de_naissance").val(),
        placeofbirth: $("#lieu_de_naissance").val(),
        address: adresse,//$('#adresse_générée').find('.address').first().html(),
        zipcode: code_postal,//$('#adresse_générée').find('.zipcode').first().html(),
        city: ville //$('#adresse_générée').find('.city').first().html()
    };

    const blob = await generatePdf(profile, "https://manu0225.gitlab.io/attestation_covid/data/certificate.pdf");

    createAndDownloadBlobFile(blob, filename = "justif " +
        new Date().toLocaleTimeString('fr-FR')
            .replaceAll(":", "-"));
    $("#btn_gen_pdf").text("PDF créé");

}