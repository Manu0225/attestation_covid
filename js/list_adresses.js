async function get_list_addresses( rayon) {
    // ON RÉCUP LA LISTE DES ADRESSES À PROXIMITÉ ET ON LA MET DANS UNE LISTE

    let list_addresses = [];//JSON.parse(data);

    await $.getJSON(`https://api.cquest.org/dvf?lat=${user_location.latitude}&lon=${user_location.longitude}&dist=${rayon}`, function (data) {
        // console.log(data)
        // console.log(list_addresses);

        var items = [];
        var k = 0;
        $.each(data, function (key, val) {
            if (key == "features") {
                $.each(val, function (key, val) {
                    list_infos = val["properties"];
                    if ("numero_voie" in list_infos &&
                        "type_voie" in list_infos &&
                        "voie" in list_infos &&
                        "code_postal" in list_infos &&
                        "commune" in list_infos &&
                        "lat" in list_infos &&
                        "lon" in list_infos
                    ) {// on teste que tous les champs sont bien définis
                        let dict = {
                            numero_voie: list_infos["numero_voie"],
                            type_voie: list_infos["type_voie"],
                            voie: list_infos["voie"],
                            code_postal: list_infos["code_postal"],
                            commune: list_infos["commune"],
                            gps_lat: list_infos["lat"],
                            gps_lon: list_infos["lon"],
                        };
                        list_addresses.push(dict);
                        // console.log(list_addresses)
                        // items.push("<li id='" + key + "'>" + val + "</li>");
                        // items.push("<li><span id='adresse_" + k + "'>" +// key=" + key + ", val=" +
                        //     "<span class='address'>" +
                        //     list_infos["numero_voie"] + " " +
                        //     list_infos["type_voie"] + " " +
                        //     list_infos["voie"] + "</span>, " +//"<br>" +
                        //     "<span class='zipcode'>" +
                        //     list_infos["code_postal"] + "</span> " +
                        //     "<span class='city'>" +
                        //     list_infos["commune"] + "</span>" +
                        //     "</span> — " +
                        //     "<span id='gps_lat_" + k + "'>" + list_infos["lat"] + "</span>, " +
                        //     "<span id='gps_lon_" + k + "'>" + list_infos["lon"] + "</span>" +

                        //     "</li>");
                        // k++;
                    }
                });
            }

        });
        // $("#liste_adresses").html(items.join(""));

        $("#génération_adresse").prop("disabled", false);
        $("#génération_adresse").text("Générer une adresse");
        // console.log("end of query")
        // console.log(list_addresses)
        // $("<ul/>", {
        //     "id": "liste_adresses",
        //     html: items.join("")
        // }).appendTo("body");
    });
    // console.log("after end of query")
    // console.log(list_addresses)
    return list_addresses;
}